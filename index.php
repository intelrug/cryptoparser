<?php
    namespace CryptoParser;
    include "simple_html_dom.php";
    $config = include "config.php";
   
    class Parser 
    {
        public $page;
        private $exchanges;
        private $markets;
        private $mysql;
        
        function Parser() {

        }

        public function getPage($url) {

            if (!empty($url)) {
                $referer = "http://www.google.com";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0");
                curl_setopt($ch, CURLOPT_REFERER, $referer);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $data = curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $this->page = ($code != 200) ? false : $data;
            }
        }

        public function getExchanges() {

            $html = str_get_html($this->page);
            $exchangesHtml = $html->find("#market-table")[0]->find("tr");

            foreach ($exchangesHtml as $exchangeHtml) {
                $exchange["name"] = $exchangeHtml->find("a")[0]->plaintext;
                $exchange["markets"] = $exchangeHtml->find(".number")[0]->plaintext;
                $exchange["24h_volume"] = str_replace(",", "", substr($exchangeHtml->find(".number")[1]->plaintext, 2));
                $updated = \DateTime::createFromFormat("d-m-Y H:i:s", trim($exchangeHtml->find("span")[0]->plaintext));
                if ($updated) $exchange["updated"] = $updated->format("Y-m-d H:i:s");

                if (!empty($exchange["name"])) {
                    $this->exchanges[] = $exchange;
                    self::insertInExchangesTable($exchange);
                }
            }
        }

        public function getExchangesMarkets($url) {
            foreach ($this->exchanges as $exchange) {

                $exchange_id = self::getExchangeIdByName($exchange['name']);
                if ($exchange_id) {
                    self::getPage($url . "/" . $exchange['name']);

                    $html = str_get_html($this->page);
                    $marketsHtml = $html->find("#market-table")[0]->find("tr");

                    foreach ($marketsHtml as $marketHtml) {
                        $market['name'] = $marketHtml->find("a")[0]->plaintext;
                        $market['pair'] = $marketHtml->find("a")[1]->plaintext;
                        $market['price'] = $marketHtml->find(".number")[0]->plaintext;
                        $market['24h_volume'] = str_replace(",", "", $marketHtml->find(".number")[1]->plaintext);

                        $calcPrice = self::parseCurrency($marketHtml->find(".number")[2]->plaintext);
                        $market['calc_price'] = $calcPrice['number'];
                        $market['calc_price_currency'] = $calcPrice['currency'];

                        $calc24hVolume = self::parseCurrency($marketHtml->find(".number")[3]->plaintext);
                        $market['calc_24h_volume'] = $calc24hVolume['number'];
                        $market['calc_24h_volume_currency'] = $calc24hVolume['currency'];

                        $market['exchange_id'] = $exchange_id;
                        
                        if (!empty($market["name"])) {
                            self::insertInMarketsTable($market);
                        }
                    }
                }
            }
        }  

        public function initDataBase($host, $username, $password, $dbname) {
            try {  
                $this->mysql = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
                $this->mysql->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e) {
                echo $e->getMessage();
            }

            $this->mysql->exec(
                "CREATE TABLE IF NOT EXISTS `exchanges` (" .
                "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," .
                "PRIMARY KEY(`id`)," .
                "`name` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL," .
                "`markets` INT NOT NULL," .
                "`24h_volume` INT NOT NULL," .
                "`updated` DATETIME NOT NULL" .
                ") ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;"
            );

            $this->mysql->exec(
                "CREATE TABLE IF NOT EXISTS `markets` (" .
                "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," .
                "PRIMARY KEY(`id`)," .
                "`exchange_id` INT UNSIGNED NOT NULL," .
                "`name` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL," .
                "`pair` VARCHAR(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL," .
                "`price` VARCHAR(16) NOT NULL," .
                "`24h_volume` INT NOT NULL," .
                "`calc_price` VARCHAR(16) NOT NULL," .
                "`calc_price_currency` VARCHAR(3) NOT NULL," .
                "`calc_24h_volume` BIGINT NOT NULL," .
                "`calc_24h_volume_currency` VARCHAR(3) NOT NULL" .
                ") ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;"
            );
        }
        
        public function insertInExchangesTable($exchange) {
            
            $sql;

            if (self::isRecordExists("exchanges", ["name" => $exchange["name"]])) {
                $sql = $this->mysql->prepare(
                    "UPDATE `exchanges` " .
                    "SET `markets` = :markets, `24h_volume` = :24h_volume, `updated` = :updated " .
                    "WHERE `name`= :name"
                );
            } else {
                $sql = $this->mysql->prepare(
                    "INSERT INTO `exchanges` (`name`, `markets`, `24h_volume`, `updated`)" .
                    "VALUES (:name, :markets, :24h_volume, :updated)"
                );
            }
            $sql->execute($exchange);
        }

        public function insertInMarketsTable($market) {

            $sql;
            $params = [
                "name" => $market["name"],
                "pair" => $market["pair"],
                "exchange_id" => $market["exchange_id"]
            ];

            if (self::isRecordExists("markets", $params)) {
                $sql = $this->mysql->prepare(
                    "UPDATE `markets` " .
                    "SET " .
                        "`price` = :price, " .
                        "`24h_volume` = :24h_volume, " .
                        "`calc_price` = :calc_price, " .
                        "`calc_price_currency` = :calc_price_currency, " .
                        "`calc_24h_volume` = :calc_24h_volume, " .
                        "`calc_24h_volume_currency` = :calc_24h_volume_currency " .
                    "WHERE " .
                        "`name`= :name, " .
                        "`pair` = :pair, " .
                        "`exchange_id` = :exchange_id"
                );
            } else {
                $sql = $this->mysql->prepare(
                    "INSERT INTO `markets` (" .
                        "`name`, " .
                        "`pair`, " .
                        "`price`, " .
                        "`24h_volume`, " .
                        "`calc_price`, " .
                        "`calc_price_currency`, " .
                        "`calc_24h_volume`, " .
                        "`calc_24h_volume_currency`, " .
                        "`exchange_id` " .
                    ") VALUES (" .
                        ":name, " .
                        ":pair, " .
                        ":price, " .
                        ":24h_volume, " .
                        ":calc_price, " .
                        ":calc_price_currency, " .
                        ":calc_24h_volume, " .
                        ":calc_24h_volume_currency, " .
                        ":exchange_id" .
                    ")"
                );
            }
            print_r($sql);
            echo "</br>";
            print_r($market);
            echo "</br>";
            $sql->execute($market);
        }

        private function isRecordExists($table, $params) {

            if (count($params) == 0) return false;

            $sqlStatement = "SELECT * FROM `$table` WHERE";
            $values = array();
            foreach ($params as $key => $value) {
                $sqlStatement .= " `$key` = ? AND";
                $values[] = $value;
            }

            $sql = $this->mysql->prepare(substr($sqlStatement, 0, -4));
            $sql->execute($values);

            return $sql->rowCount();
        }

        private function getExchangeIdByName($name) {

            $sql = $this->mysql->prepare("SELECT `id` FROM `exchanges` WHERE `name` = ?");
            $sql->execute(array($name));

            return ($sql->rowCount()) ? $sql->fetch()['id'] : false;
        }

        private function parseCurrency($currency) {
            $formatter = new \NumberFormatter('en-US', \NumberFormatter::CURRENCY);
            $formatter->setPattern("¤ #,##0.00");
            $result['number'] = $formatter->parseCurrency($currency, $result['currency']);
            return $result;
        }
    }

    $parser = new Parser();
    $parser->initDataBase($config["DB_HOST"], $config["DB_USER"], $config["DB_PASSWORD"], $config["DB_NAME"]);
    $parser->getPage($config["URL"]);
    if ($parser->page) {
        $parser->getExchanges();
    } else {
        die("Невозможно получить старницу");
    }
    $parser->getExchangesMarkets($config["URL"]);
?>